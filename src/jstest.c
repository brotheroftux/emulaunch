/* *** jstest.c *** 
*
* Joystick class tester
*
*/

#include <stdio.h>
#include "joystick.h"
#include <stdlib.h>
#include <unistd.h>

int main (int argc, char *argv[])
{
  if (argc < 2) 
  {
    printf("Usage: jstest <device>\n");
    return 1;
  }
    int s = js_open(argv[1]);
    if (s == 1) return 1;
    Joystick *js = new Joystick(s);
    unsigned char scan;
    while(1)
    {
      scan = js->read_js();
      if (scan != 129) printf("Read: %u\n", scan); // 129 == initial state of the gamepad
    }
    return 0;
}
