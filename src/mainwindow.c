/*
*
* *** MainWindow.c ***
* MainWindow class code file
* This is a part of EmuLaunch project
* Copyright brotheroftux, 2014
* This code is distributed under GPL v3 license
*
*/

#include "mainwindow.h"
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>

MainWindow::MainWindow(char device[])
{
  int s = js_open(argv[1]);
  if (s == 1) return 1;
  js_class = new Joystick(s);
}