/*
*
* *** joystick.h ***
* Joystick input class for EmuLaunch
* This is a part of EmuLaunch project
* Copyright brotheroftux, 2014
* This code is distributed under GPL v3 license
*
*/

#ifndef __JOYSTICK_H__
#define __JOYSTICK_H__

#include <sys/types.h>
#include <stdlib.h>

class Joystick 
{
  
// joy event constants, according to kernel.org joystick API

#define JOY_EVENT_INITIAL 0x80 	// initial state of the joystick
#define JOY_EVENT_BUTTON 0x01 	// button pressed
#define JOY_EVENT_AXIS 0x02 	// axis moved, perhaps we won't use it -- we don't need mini-joysticks on the gamepad

  public:
    Joystick(int fd); // constructor, fd for the joystick (call js_open before initializing the class)
    ~Joystick(); // destructor
    unsigned char read_js(); // read method, returns button number
    // struct js_event, see kernel.org joystick API
    struct js_event {
     unsigned int time; 	// event time
     short value; 		// axis value
     unsigned char type; 	// event type, see above
     unsigned char number; 	// button/axis number
    };
    // button types enumeration, used to simplify reading the buttons
    // also used to return "readable" values to owner
    // btw, i think, this buttons will be enough for the launcher
    enum JOY_BUTTONS 
    {
      BUTTON_A = 0,
      BUTTON_B,
      BUTTON_X,
      BUTTON_Y,
      BUTTON_DPAD_UP,
      BUTTON_DPAD_DOWN,
      BUTTON_DPAD_LEFT,
      BUTTON_DPAD_RIGHT
    };
  private:
    int js_fd;
    char number_of_buttons;
};

extern int js_open(char device[]); /* was the class member, now it is stand-alone function, joystick device in format of /dev/input/js0 (UNIX-like) */

#endif /* __JOYSTICK_H__ */
