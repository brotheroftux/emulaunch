/*
*
* *** EmuLaunch.c ***
* Main EmuLaunch code file
* This is a part of EmuLaunch project
* Copyright brotheroftux, 2014
* This code is distributed under GPL v3 license
*
*/

#include <stdio.h>
#include <gtk/gtk.h>

int main (int argc, char *argv[]){
  // TODO: maybe, MainWindow constructer call?
  gtk_init(&argc, &argv);
  // show the main window
  gtk_main();
  return 0;
  