/*
*
* *** MainWindow.h ***
* MainWindow class header file
* This is a part of EmuLaunch project
* Copyright brotheroftux, 2014
* This code is distributed under GPL v3 license
*
*/

#include <gtk/gtk.h>
#include "joystick.h"

#ifndef __MAINWINDOW_H__
#define __MAINWINDOW_H__

class MainWindow { // idk should it be the child of GtkWidget or not
   
  public:
    MainWindow(char device[]);
    ~MainWindow();
  private:
    void process_joystick_events(unsigned char button_number);
    void joy_init(char device[]);
    void list_fill(); // TODO: args
    void gui_init();
    Joystick *js_class;
}

#endif