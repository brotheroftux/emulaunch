/*
*
* *** joystick.c ***
* Joystick input class for EmuLaunch
* This is a part of EmuLaunch project
* Copyright brotheroftux, 2014
* This code is distributed under GPL v3 license
*
*/

// used to get joystick's name
#include <sys/ioctl.h>
// class header
#include "joystick.h"
#include <stdio.h>
#include <sys/types.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>

Joystick::Joystick (int fd)
{
 js_fd = fd; // simple, isn't it?
 ioctl(fd, JSIOCGBUTTONS, &number_of_buttons); // using ioctl to get the number of buttons
}

int js_open(char device[])
{
  int fd = open(device, O_RDONLY);
  if (fd < 0)
  {
    printf("Error opening %s\n", device);
    return 1; // error code
  } else {
    return fd;
  }
}

unsigned char Joystick::read_js()
{
  int rs; // read status
  struct js_event js_e;
  if (rs = read(js_fd, &js_e, sizeof(struct js_event)) == sizeof(struct js_event))
  {
    if (js_e.type == JOY_EVENT_BUTTON && js_e.number <= number_of_buttons){
      if (js_e.value == 1) // we need only "pressed" state
	return js_e.number;
      else return 129;
    }else if (js_e.type == JOY_EVENT_AXIS && js_e.number <= 10){
      return 129; // like, initial joystick state. there are no gamepads with 129 buttons, lol
    }
    return 129;
  }
}